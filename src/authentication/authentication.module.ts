import { Module } from '@nestjs/common';
import { AuthenticationController } from './authentication.controller';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import configuration from '../config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
    }),
    HttpModule],
  controllers: [AuthenticationController],
})
export class AuthenticationModule {}