import { HttpService } from '@nestjs/axios';
import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {AxiosResponse} from 'axios';
import {Observable} from 'rxjs';
import { LoginDto } from './dto/login.dto';

@Controller('authentication')
export class AuthenticationController {
  constructor(private httpService: HttpService,private configurationService: ConfigService) {}

  @Post()
  async login(@Body() loginDto: LoginDto): Promise<any> {
    const baseurl = this.configurationService.get<string>('auth_service');
    const res = await this.httpService.post(baseurl+'/auth',loginDto).toPromise();
    return res.data;
  }

  @Get()
  getHello(): string {
    return "Authentication";
  }
}
