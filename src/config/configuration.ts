require('dotenv').config()
export default () => ({
  port: parseInt(process.env.PORT),
  x_client: process.env.X_CLIENT,
  x_service: process.env.X_SERVICE,
  auth_service: process.env.AUTHENTICATION_SERVICE,
  fwd_service: process.env.FWD_SERVICE,
});