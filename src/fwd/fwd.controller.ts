import { HttpService } from '@nestjs/axios';
import { Body, Controller, Post } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {AxiosResponse} from 'axios';
import {Observable} from 'rxjs';
import { GetProductDto } from './dto/get-product.dto';

@Controller('fwd')
export class FwdController {
  constructor(private readonly httpService: HttpService,private readonly configurationService: ConfigService) {}

  @Post('get/product')
 async getProduct(@Body() getProduct: GetProductDto): Promise<any>{
    const baseurl = this.configurationService.get<string>('fwd_service');
    const res = await this.httpService.post(baseurl + '/fwd/getProduct', getProduct).toPromise();
    return res.data;
  }
}
