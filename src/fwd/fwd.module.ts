import { Module } from '@nestjs/common';
import {FwdController} from './fwd.controller';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import configuration from '../config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
    }),
    HttpModule],
  controllers: [FwdController],
})
export class FwdModule {}